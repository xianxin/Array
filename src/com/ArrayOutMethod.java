package com;

public class ArrayOutMethod {
/*	public static void main(String[] args) {
		 int[] a; 
		 * int[] b = { 3, 5, 9, 7, 11, 13, 15 }; 
		 * a=b.clone(); 
		 * out(b);
		 * b[3]=99; 
		 * out(a);
		 

		int[] a = new int[20];
		a[0] = 1;
		a[1] = 1;
		for (int i = 2; i < 20; i++)
			a[i] = a[i - 2] + a[i - 1];
		out(a);
	}*/

	public static void out(int[] a) {
		int b = 0;
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + "\t");
			b++;
			if (b % 5 == 0)
				System.out.println();
		}
	}
}
